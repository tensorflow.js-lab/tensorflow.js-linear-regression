const tf = require('@tensorflow/tfjs');
const _  = require('lodash');

class LogisticRegression {
    constructor(features, labels, options) {
        this.features = this.processFeatures(features);
        this.labels = tf.tensor(labels);
        this.mseHistory = [];
        this.costHisory = [];
        // Assign options with default values
        this.options =
            Object.assign({
                learningRate: 0.1,
                iterations: 1000,
                batchSize: 10,
                decisionBoundary: 0.5
            }, options);
        this.weights = tf.zeros([
            this.features.shape[1],this.labels.shape[1]
        ]);
    }
    train() {
        const batchQuantity = Math.floor(this.features.shape[0] / this.options.batchSize);

        for (let i = 0; i < this.options.iterations ; i++) {
            for (let j = 0; j < batchQuantity; j++) {
                const startIndex = j * this.options.batchSize;
                const { batchSize } = this.options;

                const featureSlice = this.features.slice(
                    [startIndex, 0],
                    [batchSize, -1]
                )
                const labelSlice = this.labels.slice(
                    [startIndex, 0],
                    [batchSize, -1]
                )
                this.gradientDescent(featureSlice, labelSlice);
            }
            this.recordCost();
            this.updateLearningRate();
        }
    }
    test(testFeatures, testLabels) {
        const predictions = this.predict(testFeatures);
        testLabels = tf.tensor(testLabels).argMax(1);

        const incorrect = predictions
            .notEqual(testLabels)
            .sum()
            .get();

        return (predictions.shape[0] - incorrect) / predictions.shape[0];
    }
    gradientDescent(features, labels) {
        const currentGuesses = features.matMul(this.weights)
            .softmax();
        const differences = currentGuesses.sub(labels);

        const slopes = features
            .transpose()
            .matMul(differences)
            .div(features.shape[0]);

        this.weights = this.weights.sub(slopes.mul(this.options.learningRate));
    }
    processFeatures(features) {
        features = tf.tensor(features);

        if (this.mean && this.variance){
            features = features.sub(this.mean).div(this.variance.pow(0.5));
        } else {
            features = this.standardize(features);
        }

        features = tf.ones([features.shape[0], 1]).concat(features, 1);

        return features;
    }
    standardize(features) {
        const {mean, variance} = tf.moments(features);

        const filler = variance
            .cast('bool')
            .logicalNot()
            .cast('float32');

        this.mean = mean;
        this.variance = variance.add(filler);

        return features.sub(mean).div(this.variance.pow(0.5));
    }

    recordMSE() {
        const mse = this.features
            .matMul(this.weights)
            .sub(this.labels)
            .pow(2)
            .sum()
            .div(this.features.shape[0])
            .get();
        this.mseHistory.unshift(mse);
    }
    recordCost() {
        const gusses = this.features
            .matMul(this.weights)
            .softmax();
        const termOne = this.labels
            .transpose()
            .matMul(gusses.add(1e-7).log());

        const termTow =
            this.labels
                .mul(-1)
                .add(1)
                .transpose()
                .matMul(
                  gusses
                      .mul(-1)
                      .add(1)
                      .add(1e-7) // avoid log(0)
                      .log()
                );
        const cost = termOne.add(termTow)
            .div(this.features.shape[0])
            .mul(-1)
            .get(0, 0);

        this.costHisory.unshift(cost);
    }
    updateLearningRate() {
        if (this.costHisory.length < 2) {
            return;
        }
        if (this.costHisory[0] > this.costHisory[1]) {
            this.options.learningRate /= 2;
        } else {
            this.options.learningRate *= 1.05;
        }
    }
    predict(observations) {
        return this.processFeatures(observations).matMul(this.weights)
            .softmax()
            .argMax(1);
    }
}

module.exports = LogisticRegression;
